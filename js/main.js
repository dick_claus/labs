( function() {
        
        // jquery is already loaded globally
        define('jquery', function() {
            return window.jQuery;
        });

        require(["jquery", "jquery.alpha", "jquery.beta"], function($) {
            //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
            $(function() {
                $('body').alpha().beta();
            });
        });
    }());
