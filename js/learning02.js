function Rectangle(width, height) {
    this.width = width;
    this.height = height;
}

Rectangle.prototype.getArea = function getArea() {
    return this.width * this.height;
};
Rectangle.prototype.getPerimeter = function getPerimeter() {
    return 2 * (this.width + this.height);
};
Rectangle.prototype.toString = function toString() {
    return this.constructor.name + " a=" + this.getArea() + " p=" + this.getPerimeter();
};

function Square(side) {
    this.width = side;
    this.height = side;
}

// Make Square inherit from Rectangle
Square.prototype = Object.create(Rectangle.prototype, {
    constructor : {
        value : Square
    }
});
// Override a method
Square.prototype.getPerimeter = function getPerimeter() {
    return this.width * 4;
}; 

var rect = new Rectangle(6, 4);
var sqr = new Square(5);
console.log(rect.toString())
console.log(sqr.toString())

var Rectangle2 = {
  name: "Rectangle2",
  getArea: function getArea() {
    return this.width * this.height;
  },
  getPerimeter: function getPerimeter() {
    return 2 * (this.width + this.height);
  },
  toString: function toString() {
    return this.name + " a=" + this.getArea() + " p=" + this.getPerimeter();
  }
};

var Square2 = Object.create(Rectangle2);
Square2.name = "Square2";
Square2.getArea = function getArea() {
  return this.width * this.width;
};
Square2.getPerimeter = function getPerimeter() {
  return this.width * 4;
};

var rect2 = Object.create(Rectangle2);
rect2.width = 6;
rect2.height = 4;
var square2 = Object.create(Square2);
square2.width = 5;
console.log(rect2.toString());
console.log(square2.toString());

// another example of creating objects.

function Controller(model, view) {
  view.update(model.value);
  return {
    up: function onUp(evt) {
      model.value++;
      view.update(model.value);
    },
    down: function onDown(evt) {
      model.value--;
      view.update(model.value);
    },
    save: function onSave(evt) {
      model.save();
      view.close();
    }
  };
}

var on = Controller(
  // Inline a mock model
  {
    value: 5,
    save: function save() {
      console.log("Saving value " + this.value + " somewhere");
    }
  },
  // Inline a mock view
  {
    update: function update(newValue) {
      console.log("View now has " + newValue);
    },
    close: function close() {
      console.log("Now hiding view");
    }
  }
);
setTimeout(on.up, 100);
setTimeout(on.down, 200);
setTimeout(on.save, 300);
